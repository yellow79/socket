package socket

import (
	"testing"
)

func TestRouter(t *testing.T) {
	r := &Router{}
	r.handlers = make(map[string]RequestHandler)
	r.addRoute("valid:handler", func(d ReqData) (res ResData, err error) {
		res = ResData{}
		res["stringValue"] = "test"
		res["intValue"] = 1

		return
	})

	var handler_1 RequestHandler = r.getHandler("notfound:handler")
	res, err := handler_1(nil)
	if err.Error() != "404:NotFound" {
		t.Error("err should be not nil")
	}

	var handler_2 RequestHandler = r.getHandler("")
	res, err = handler_2(nil)
	if err.Error() != "400:BadRequest" {
		t.Error("err should be not nil")
	}

	var handler_3 RequestHandler = r.getHandler("valid:handler")
	res, err = handler_3(nil)
	if err != nil {
		t.Error("should be nil")
	}
	if res["stringValue"] != "test" {
		t.Error("should be string('test')")
	}
	if res["intValue"] != 1 {
		t.Error("should be int(1)")
	}
}