package socket

import (
	"fmt"
)

type IRouter interface {
	addRoute(method string, cb RequestHandler)
	getHandler(method string) RequestHandler
}
type ReqData  map[string]interface{}
type ResData  map[string]interface{}
type ResError struct {
	Code int `json:"code"`
	Message string `json:"message"`
}
func (e ResError) Error() string {
	return fmt.Sprintf("%d:%s", e.Code, e.Message)
}

type RequestHandler func(req ReqData)(res ResData, err error)

type Router struct {
	handlers map[string]RequestHandler
}
func(r *Router) addRoute(method string, cb RequestHandler) {
	if r.handlers == nil {
		r.handlers = make(map[string]RequestHandler)
	}
	r.handlers[method] = cb
}
func(r *Router) getHandler(method string) RequestHandler {
	handler, ok := r.handlers[method]
	if !ok {
		if method == "" {
			handler = errorHandler(400, "BadRequest")
		} else {
			handler = errorHandler(404, "NotFound")
		}
	}
	return handler
}

func errorHandler(code int, message string) RequestHandler {
	return func(req ReqData) (res ResData, err error) {
		return nil, getError(code, message)
	}
}
func getError(code int, message string) error {
	return  ResError{code, message}
}