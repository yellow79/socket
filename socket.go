package socket

import (
	"log"
	"strconv"
	"net"
)

type IServer interface {
	Use(method string, cb RequestHandler)
	Run(port int)
}
type server struct {
	ns string
	workers int
	router IRouter
}
func(s *server) Use(method string, cb RequestHandler) {
	s.router.addRoute(s.ns + ":" + method, cb)
}
func(s *server) Run(port int) {
	server, err := net.Listen("tcp", ":" + strconv.Itoa(port))
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("Socket server started at port %d", port)
	defer server.Close()

	for {
		c, err := server.Accept()
		if err != nil {
			log.Fatal(err)
		}
		go processClient(c, s.router, s.workers)
	}
}
func New(ns string, n int, m ...LoggerHandler )IServer {
	if len(m) > 0 {
		logger = m[0]
	}
	s := &server{}
	s.ns = ns
	s.workers = n
	s.router = &Router{make(map[string]RequestHandler)}
	return s
}
type LoggerHandler func(method string, time int64, err error)
var logger LoggerHandler = func (method string, time int64, err error) {}