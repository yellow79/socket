#### Data exchange between servers through TCP protocol

## How it use

### Server code
    package main
    
    import (
        "fmt"
    	"flag"
    	"bitbucket.org/yellow79/socket"
    )
    var (
    	PORT = flag.Int("port", 2000, "port")
    	WORKERS = flag.Int("workers", 3, "workers")
    )
    func init(){
    	flag.Parse()
    }
    func Logger(method string, time int64, err error) {
    	if err != nil {
    		method = err.Error()
    	}
    	// Also here you can use timing and increment metrics
    	fmt.Printf("%s\t%d\n", method, time)
    }
    func main() {    
    	s := socket.New("TEST", *WORKERS, Logger) // namespace & number of workers & function for logging requests
    	s.Use("ping", func(req socket.ReqData)(socket.ResData, error) {    
    		res := socket.ResData{} // it is simple map[string]interface{} 
    		res["message"] = "pong"
    		res["anyField"] = "someValue"
    		res["arrayValue"] = []int{45,78,21}
    		return res, nil
    	})
    	s.Use("error:handler", func(req socket.ReqData)(socket.ResData, error) {    
            return nil, socket.ResError{2000124, "Error Description"}
        })
    	s.Run(*PORT)
    }
    
### TEST
    telnet 127.0.0.1 2000
    
    > {"id":"1", "method": "TEST:ping"}
    // will return {"id":"1","error":null,"data":{"message":"pong","anyField":"someValue","arrayValue":[45,78,21]}}
    
    > {"id":"45621", "method": "TEST:error:handler"}
    // will return {"id":"45621","data":null,"error":{"code":2000124,"message":"Error Description"}}
    
    > {}
    // will return {"id":"","data":null,"error":{"code":400,"message":"BadRequest"}}
    
    > {"id":"45622", "method": "notfound:handler"}
    // will return {"id":"1","data":null,"error":{"code":404,"message":"NotFound"}}
    
    > {"id":"-1", "method": "TEST:ping"}
    // if requestID == -1 server will return nothing

