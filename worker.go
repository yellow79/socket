package socket

import (
	"net"
	"encoding/json"
	"bufio"
	"io"
	"time"
)

type ServerRequest struct {
	Id string `json:"id"`
	Method string `json:"method"`
	Data ReqData `json:"data"`
	Timestamp time.Time `json:"-"`
}
type ServerResponse struct {
	Id string `json:"id"`
	Data ResData `json:"data"`
	Error error `json:"error"`
}
func processRequest(w io.Writer, ch <- chan ServerRequest, r IRouter) {
	for req := range ch {
		handler := r.getHandler(req.Method)
		resData, resErr := handler(req.Data)
		resItem := &ServerResponse{req.Id, resData, resErr}

		if req.Id != "-1" {
			response, _ := json.Marshal(resItem)
			w.Write(append(response, '\n'))
		}
		go logger(req.Method, int64(time.Since(req.Timestamp).Nanoseconds() / 1000), resErr)
	}
}
func processClient(client net.Conn, router IRouter, numWorkers int) {
	requests := make(chan ServerRequest, numWorkers)
	for i := 0; i < numWorkers; i++ {
		go processRequest(client, requests, router)
	}
	for {
		line, err := bufio.NewReader(client).ReadString('\n')
		if err != nil {
			if err == io.EOF {
				break
			}
			continue
		}
		req := &ServerRequest{Timestamp: time.Now()}
		json.Unmarshal([]byte(line), req)

		requests <- *req
	}
	close(requests)
}